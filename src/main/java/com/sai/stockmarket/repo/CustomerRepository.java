package com.sai.stockmarket.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sai.stockmarket.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

	Optional<Customer> findByEmail(String email);
	Optional<Customer> findByAadharCardId(String aadharCardId);
	Optional<Customer> findByPanCardId(String panCardId);
	Optional<Customer> findByMobile(String mobile);
}
