package com.sai.stockmarket.payload.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ErrorMessage {
	//payload means request/response object
	
	private String dateTime;
	private String message;
	private int status;
	private String code;

}
