package com.sai.stockmarket.handler;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.sai.stockmarket.exception.CustomerNotFoundException;
import com.sai.stockmarket.payload.response.ErrorMessage;

//If any exception comes in rest contoller comes to here
@RestControllerAdvice
public class GlobalExceptionHandler {

	/***
	 * If CustommerNotFoundException is thrown from any where in the RestController 
	 * then below method is executed and Returns Error Message with 500 Status code.
	 * It is like a Reusable Catch block code.
	 */
	
	@ExceptionHandler(CustomerNotFoundException.class)
	public ResponseEntity<ErrorMessage> handleCustomerNotFoundException(CustomerNotFoundException cnfe){

		return ResponseEntity.internalServerError().body(
				new ErrorMessage(
						new Date().toString(),
						cnfe.getMessage(),
						HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR.name()
						)
				);
	} 
}
