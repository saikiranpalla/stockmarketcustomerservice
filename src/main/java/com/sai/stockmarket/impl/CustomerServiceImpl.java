package com.sai.stockmarket.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sai.stockmarket.entity.Customer;
import com.sai.stockmarket.exception.CustomerNotFoundException;
import com.sai.stockmarket.repo.CustomerRepository;
import com.sai.stockmarket.service.ICustomerService;

@Service
public class CustomerServiceImpl implements ICustomerService {

	@Autowired
	private CustomerRepository repo;

	@Override
	public Long saveCustomer(Customer customer) {
		return repo.save(customer).getId();
	}

	@Override
	public Customer getOneCustomer(Long id) {

		Optional<Customer> opt = repo.findById(id);

		if(opt.isEmpty()) { throw new
			CustomerNotFoundException("Customer '"+id+"'is not found"); }else { return
					opt.get(); }

	}

	@Override
	public Customer getOneCustomerByEmail(String email) {

		Optional<Customer> opt =repo.findByEmail(email); 
		/*if(opt.isEmpty()) { throw
		  new CustomerNotFoundException(); }else { return opt.get(); }*/

		return validateInput(opt, email);

	}

	@Override
	public Customer getOneCustomerByMobile(String mobile) {
		Optional<Customer> opt = repo.findByMobile(mobile);
		/*
		 * if(opt.isEmpty()) { throw new CustomerNotFoundException(); }else { return
		 * opt.get(); }
		 */
		return validateInput(opt, mobile);
	}

	@Override
	public Customer getOneCustomerByPanCard(String panCard) {
		Optional<Customer> opt = repo.findByPanCardId(panCard);
		/*
		 * if(opt.isEmpty()) { throw new CustomerNotFoundException(); }else { return
		 * opt.get(); }
		 */
		return validateInput(opt, panCard);
	}

	@Override
	public Customer getOneCustomerByAadhar(String aadhar) {
		Optional<Customer> opt = repo.findByAadharCardId(aadhar);
		/*
		 * if(opt.isEmpty()) { throw new CustomerNotFoundException(); }else { return
		 * opt.get(); }
		 */
		return validateInput(opt, aadhar);
	}

	@Override
	public void updateCustomer(Customer customer) {
		Long id = customer.getId();
		if(id != null && repo.existsById(id)) {
			repo.save(customer);
		}else {
			throw new CustomerNotFoundException("Given Customer '"+id+"' not exist");
		}
	}

	@Override
	public void deleteCustomer(Long id) {
		repo.delete(getOneCustomer(id));

	}

	@Override
	public List<Customer> getAllCustomers() {
		return repo.findAll();
	}

	private Customer validateInput(Optional<Customer> opt,String input) {
		/*
		 * if(id != null && repo.existsById(id)) { repo.save(customer); }else { throw
		 * new CustomerNotFoundException("Given Customer '"+id+"' not exist"); }
		 */
		return opt.orElseThrow(()-> new CustomerNotFoundException("Customer '"+input+"' not found"));
	}
}
