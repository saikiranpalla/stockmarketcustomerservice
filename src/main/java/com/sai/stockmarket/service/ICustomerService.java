package com.sai.stockmarket.service;

import java.util.List;

import com.sai.stockmarket.entity.Customer;

public interface ICustomerService {
	
	Long saveCustomer(Customer customer);
	Customer getOneCustomer(Long id);
	Customer getOneCustomerByEmail(String email);
	Customer getOneCustomerByMobile(String mobile);
	Customer getOneCustomerByPanCard(String panCard);
	Customer getOneCustomerByAadhar(String aadhar);
	void updateCustomer(Customer customer);
	void deleteCustomer(Long id);
	List<Customer> getAllCustomers();
	
}
