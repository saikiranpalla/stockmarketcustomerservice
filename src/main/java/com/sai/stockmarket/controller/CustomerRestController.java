package com.sai.stockmarket.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sai.stockmarket.entity.Customer;
import com.sai.stockmarket.exception.CustomerNotFoundException;
import com.sai.stockmarket.service.ICustomerService;

import lombok.extern.slf4j.Slf4j;

@RequestMapping("/customer")
@RestController
@Slf4j
public class CustomerRestController {
	
	@Autowired
	private ICustomerService service;
	
	//save customer
	@PostMapping("/save")
	public ResponseEntity<String> createCustomer(
			@RequestBody Customer customer
			){
		log.info("ENTERED INTO SAVE METHOD");
		ResponseEntity<String> response = null;
		try {
			Long id = service.saveCustomer(customer);
			response = ResponseEntity.ok("Customer is created with '"+id+"'");
			log.info("CUSTOMER IS CREATED {}.",id);
		}catch(Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		log.info("ABOUT TO LEAVE SAVE METHOD");
		return response;
	}
	
	//fetch all customers
	@GetMapping("/all")
	public ResponseEntity<List<Customer>> getAllCustomers(){
		ResponseEntity<List<Customer>> response = null;
		log.info("ENTERED INTO FETCH ALL METHOD");
		try {
			List<Customer> list =service.getAllCustomers();
			response = ResponseEntity.ok(list);
			log.info("FETCH ALL METHOD IS SUCCESS");
		}catch(Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());	
		}
		return response;
	} 
	
	//fetch customer by email
	@GetMapping("/fetch/{mail}")
	public ResponseEntity<Customer> getCustomerByEmail(@PathVariable String mail){
		ResponseEntity<Customer> response = null;
		log.info("ENTERED INTO GET CUSTOMER BY EMAIL");
		try {
			Customer cust = service.getOneCustomerByEmail(mail);
			response =  new ResponseEntity<Customer>(cust,HttpStatus.OK);
			log.info("FETCH ONE CUSTOMER Y EMAIL METHID IS SUCCESS");
		} catch (CustomerNotFoundException cnfe) {
			cnfe.printStackTrace();
			log.error(cnfe.getMessage());
			throw cnfe;
		}
		log.info("ABOUT TO LEAVE GET CUSTOMER BY EMAIL METHOD");
		return response;
	}
	
	//fetch customer by mobile
	@GetMapping("/fetch/{mobile}")
	public ResponseEntity<Customer> getCustomerByMobile(@PathVariable String mobile){
		log.info("ENTERED INTO GET CUSTOMER BY MOBILE");
		ResponseEntity<Customer> response = null;
		try {
			Customer cust = service.getOneCustomerByMobile(mobile);
			response = new ResponseEntity<Customer>(cust,HttpStatus.OK);
			log.info("FETCH ONE CUSTOMER BY MOBILE METHID IS SUCCESS");
		} catch (CustomerNotFoundException cnfe) {
			cnfe.printStackTrace();
			log.error(cnfe.getMessage());
			throw cnfe;
		}
		log.info("ABOUT TO LEAVE GET CUSTOMER BY MOBILE METHOD");
		return response;
	}
	
	//fetch customer  Aadhar
	@GetMapping("fetch/{aadhar}")
	public ResponseEntity<Customer> getCustomerByAadhar(@PathVariable String aadhar){
		ResponseEntity<Customer> response = null;
		log.info("ENTERED INTO FETCH CUSTOMER BY AADHAR METHOD");
		try {
			Customer cust = service.getOneCustomerByAadhar(aadhar);
			response = new ResponseEntity<Customer>(cust,HttpStatus.OK);
			log.info("FETCH ONE CUSTOMER BY AADHAR METHID IS SUCCESS");
		} catch (CustomerNotFoundException cnfe) {
			cnfe.printStackTrace();
			log.error(cnfe.getMessage());
			throw cnfe;
		}
		return response;
	}
	
	//fetch Customer by pancard
	@GetMapping("/fetch/{pancard}")
	public ResponseEntity<Customer> getCustomerByPancard(@PathVariable String pancard){
		ResponseEntity<Customer> response = null;
		log.info("ENTERED INTO FETCH CUSTOMER BY PANCARD METHOD");
		try {
			Customer cust = service.getOneCustomerByPanCard(pancard);
			response = new ResponseEntity<Customer>(cust,HttpStatus.OK);
			log.info("FETCH ONE CUSTOMER BY AADHAR PANCARD IS SUCCESS");
		} catch (CustomerNotFoundException cnfe) {
			cnfe.printStackTrace();
			log.error(cnfe.getMessage());
			throw cnfe;
		}
		return response;
	}
	
	
	//update customer
	@PutMapping("/modify")
	public ResponseEntity<String> updateCustomer(@RequestBody Customer customer){
		ResponseEntity<String> response = null;
		log.info("ENTERED INTO CUSTOMER UPDATE METHOD");
		try {
			service.updateCustomer(customer);
			response = ResponseEntity.ok("CUSTOMER UPDATEd");
			log.info("UPDATE CUSTOMER METHOD IS SUCCESS");
		} catch (CustomerNotFoundException cnfe) {
			cnfe.printStackTrace();
			log.error(cnfe.getMessage());
			throw cnfe;
		}
		log.info("ABOUT TO LEAVE UPDATE METHOD");
		return response;
	}
	
	//delete customer 
	@DeleteMapping("/remove/{id}")
	public ResponseEntity<String> deleteCustomer(@PathVariable Long id){
		log.info("ENTERED INTO CUSTOMER DELETE METHOD");
		ResponseEntity<String> response = null;
		try {
			service.deleteCustomer(id);
			response = ResponseEntity.ok("CUSTOMER DELETED");
			log.info("DELETE CUSTOMER METHOD IS SUCCESS");
		} catch (CustomerNotFoundException cnfe) {
			cnfe.printStackTrace();
			log.error(cnfe.getMessage());
			throw cnfe;
		}
		return response;
	}
	
}
